﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator anim;
    public bool isGrounded;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Horizontal"))
        {
            anim.SetBool("run", true);
            anim.SetBool("stop", false);
        }
        if(Input.anyKey == false)
        {
            anim.SetBool("run", false);
            anim.SetBool("stop", true);
        }
        if(Input.GetButtonDown("Jump"))
        {
            if (isGrounded == true)
             {
                anim.SetTrigger("jump");
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 15), ForceMode2D.Impulse);
                isGrounded = false;
             }
        }
    }
 
     void OnCollisionEnter2D(Collision2D coll)
     {
         if (coll.gameObject.tag == "lantainya")
         {
             isGrounded = true;
         }
     }
}